module axi_slave( aclk, aresetn, awaddr, awvalid, awready, wdata, wvalid, wready, bresp, bvalid, bready, araddr, arvalid, arready, rdata, rvalid, rready );
  
  input wire aclk;
  input wire aresetn;
  
  output wire awready;
  output wire wready;
  output wire [1:0] bresp;
  output wire bvalid;
  
  output wire arready;
  output wire rvalid;
  output wire [31:0] rdata;
  
  input wire [31:0] awaddr;
  input wire awvalid;
  input wire wvalid;
  input wire [31:0] wdata;
  input wire bready;
  input wire [31:0] araddr;
  input wire arvalid;
  input wire rready;
  
  axi_slave_write slave_write(
                    .aclk(aclk),
                    .aresetn(aresetn),
                    
                    .awaddr(awaddr),
                    .awvalid(awvalid),
                    .awready(awready),
                    
                    .wdata(wdata),
                    .wvalid(wvalid),
                    .wready(wready),
                    
                    .bresp(bresp),
                    .bready(bready),
                    .bvalid(bvalid)
  );
  
  axi_slave_read slave_read(
                    .aclk(aclk),
                    .aresetn(aresetn),
                    
                    .araddr(araddr),
                    .arvalid(arvalid),
                    .arready(arready),
                    
                    .rdata(rdata),
                    .rvalid(rvalid),
                    .rready(rready)
  );
  
endmodule 