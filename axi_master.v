module axi_master( aclk, aresetn, awaddr, awvalid, awready, wdata, wvalid, wready, bresp, bvalid, bready, araddr, arvalid, arready, rdata, rvalid, rready );
  
  input wire aclk;
  input wire aresetn;
  
  input wire awready;
  input wire wready;
  input wire [1:0] bresp;
  input wire bvalid;
  
  input wire arready;
  input wire rvalid;
  input wire [31:0] rdata;
  
  output wire [31:0] awaddr;
  output wire awvalid;
  output wire wvalid;
  output wire [31:0] wdata;
  output wire bready;
  output wire [31:0] araddr;
  output wire arvalid;
  output wire rready;
  
  axi_master_write master_write(
                    .aclk(aclk),
                    .aresetn(aresetn),
                    
                    .awaddr(awaddr),
                    .awvalid(awvalid),
                    .awready(awready),
                    
                    .wdata(wdata),
                    .wvalid(wvalid),
                    .wready(wready),
                    
                    .bresp(bresp),
                    .bready(bready),
                    .bvalid(bvalid)
  );
  
  axi_master_read master_read(
                    .aclk(aclk),
                    .aresetn(aresetn),
                    
                    .araddr(araddr),
                    .arvalid(arvalid),
                    .arready(arready),
                    
                    .rdata(rdata),
                    .rvalid(rvalid),
                    .rready(rready)
  );
  
endmodule 