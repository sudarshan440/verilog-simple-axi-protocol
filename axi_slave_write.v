module axi_slave_write( aclk, aresetn, awaddr, awready, awvalid,
                  wdata, wvalid, wready, bresp, bvalid, bready);
  
  input aclk;
  input aresetn;
  output awready;
  output wready;
  output bresp;
  output bvalid;
  
  input awaddr;
  input awvalid;
  input wdata;
  input wvalid;
  input bready;
  
  parameter BRESP_WIDTH = 2;
  parameter ADDR_WIDTH = 32;
  parameter DATA_WIDTH = 32;
  parameter MEM_WIDTH = 32;
  parameter MEM_DEPTH = 1024;
  
  parameter IDLE=4'b0001;
  parameter ADDR=4'b0010;
  parameter DATA=4'b0100;
  parameter RESP=4'b1000;
  
  wire aclk;
  wire aresetn;
  reg awready;
  reg wready;
  reg [BRESP_WIDTH-1:0] bresp;
  reg bvalid;
  
  wire [ADDR_WIDTH-1:0] awaddr;
  wire [DATA_WIDTH-1:0] wdata;
  wire awvalid;
  wire wvalid;
  wire bready;
  
  reg [3:0] state;
  reg [3:0] next_state;
  
  wire start_trans;
  reg addr_done;
  reg data_done;
  reg resp_done;
  reg [BRESP_WIDTH-1:0] temp_resp;
  
  reg [MEM_WIDTH-1:0] mem_wdata;
  reg [ADDR_WIDTH-1:0] mem_waddr;
  reg [MEM_WIDTH-1:0] memory [MEM_DEPTH];
  
  assign start_trans = awvalid;
  
  integer i;
  initial begin
    addr_done <= 0;
    data_done <= 0;
    resp_done <= 0;
    awready <= 0;
    wready <= 0;
    bvalid <= 0;
  end
  
  always@(*) begin
    memory[mem_waddr] <= mem_wdata;
  end
  
  
  always@( posedge aclk, negedge aresetn ) begin
    if( !aresetn ) begin
      state <= IDLE;
    end else begin
      state <= next_state;
    end
  end
  
  always@(state or start_trans or addr_done or data_done or resp_done or awvalid or wvalid or bready ) begin
    case(state) 
      IDLE: begin
        resp_done <= 0;
        bvalid <= 0;
        next_state <= (start_trans)?ADDR : IDLE;
      end
      ADDR: begin
        awready <= 1;
        mem_waddr <= awaddr;
        addr_done <= 1;
        next_state <= (addr_done )? DATA : ADDR;
      end
      DATA: begin
        awready <= 0;
        wready <= 1;
        if(  wvalid ) begin
          mem_wdata <= wdata;
          data_done <= 1;
        end else begin
          data_done <= 0;
        end
        next_state <= (data_done )? RESP : DATA;
      end
      RESP: begin
        wready <=0;
        bvalid <= 1;
        bresp <= 0;
        if( bready  ) begin
          resp_done <= 1;
        end else begin
          resp_done <= 0;
        end
        next_state <= (resp_done )? IDLE : RESP;
      end
      default : begin
        next_state <= IDLE;
      end
    endcase 
  end
  
endmodule 
 