module axi_slave_read( aclk, aresetn, araddr, arready, arvalid,
                  rdata, rvalid, rready);
  
  input aclk;
  input aresetn;
  output arready;
  input rready;
  
  input araddr;
  input arvalid;
  output rdata;
  output rvalid;
  
  parameter ADDR_WIDTH = 32;
  parameter DATA_WIDTH = 32;
  parameter MEM_WIDTH = 32;
  parameter MEM_DEPTH = 1024;
  
  parameter IDLE=4'b0001;
  parameter ADDR=4'b0010;
  parameter DATA=4'b0100;
  
  wire aclk;
  wire aresetn;
  reg arready;
  wire rready;
  
  wire [ADDR_WIDTH-1:0] araddr;
  reg [DATA_WIDTH-1:0] rdata;
  wire arvalid;
  reg rvalid;
  
  reg [3:0] state;
  reg [3:0] next_state;
  
  wire start_trans;
  reg addr_done;
  reg data_done;
  
  reg [MEM_WIDTH-1:0] mem_rdata;
  reg [ADDR_WIDTH-1:0] mem_raddr;
  reg [MEM_WIDTH-1:0] memory [MEM_DEPTH];
  
  assign start_trans = arvalid;
  
  integer i;
  initial begin
    addr_done <= 0;
    data_done <= 0;
    arready <= 0;
    rvalid <= 0;
    for( i = 0; i< MEM_DEPTH; i= i+1) begin
      memory[i] <= i;
    end
  end
  
  always@(*) begin
    mem_rdata <= memory[mem_raddr];
  end
  
  
  always@( posedge aclk, negedge aresetn ) begin
    if( !aresetn ) begin
      state <= IDLE;
    end else begin
      state <= next_state;
    end
  end
  
  always@(state or start_trans or addr_done or data_done or arvalid or rready ) begin
    case(state) 
      IDLE: begin
        rvalid <= 0;
        next_state <= (start_trans)?ADDR : IDLE;
      end
      ADDR: begin
        arready <= 1;
        mem_raddr <= araddr;
        addr_done <= 1;
        next_state <= (addr_done )? DATA : ADDR;
      end
      DATA: begin
        arready <= 0;
        rvalid <= 1;
        rdata <= mem_rdata;
        if(  rready ) begin
          data_done <= 1;
        end else begin
          data_done <= 0;
        end
        next_state <= (data_done )? IDLE : DATA;
      end
      default : begin
        next_state <= IDLE;
      end
    endcase 
  end
  
endmodule 
 