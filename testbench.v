// Code your testbench here
// or browse Examples
module axi_tb;
  
  reg aclk;
  reg aresetn;
  
  wire [1:0] bresp;
  wire  bvalid;
  
  wire awready;
  wire wready;
  
  wire [31:0] awaddr;
  wire [31:0] wdata;
  wire awvalid;
  wire wvalid;
  wire bready;
  
  wire [31:0] araddr;
  wire [31:0] rdata;
  wire  rready;
  wire rvalid;
  wire arready;
  wire arvalid;
  
  axi_master master(.aclk(aclk),
                    .aresetn(aresetn),
                    
                    .awaddr(awaddr),
                    .awvalid(awvalid),
                    .awready(awready),
                    
                    .wdata(wdata),
                    .wvalid(wvalid),
                    .wready(wready),
                    
                    .bresp(bresp),
                    .bready(bready),
                    .bvalid(bvalid),
                    
                    .araddr(araddr),
                    .arvalid(arvalid),
                    .arready(arready),
                    
                    .rdata(rdata),
                    .rvalid(rvalid),
                    .rready(rready)
                   );
  
  axi_slave slave(.aclk(aclk),
                    .aresetn(aresetn),
                    
                    .awaddr(awaddr),
                    .awvalid(awvalid),
                    .awready(awready),
                    
                    .wdata(wdata),
                    .wvalid(wvalid),
                    .wready(wready),
                    
                    .bresp(bresp),
                    .bready(bready),
                    .bvalid(bvalid),
                  
                    .araddr(araddr),
                    .arvalid(arvalid),
                    .arready(arready),
                    
                    .rdata(rdata),
                    .rvalid(rvalid),
                    .rready(rready)
                   );
  
  initial begin
    aclk <= 0;
    aresetn <= 1;
    forever begin
      #5 aclk <= !aclk;
    end
    
  end
  
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0, axi_tb);
    #10;
    aresetn <= 0;
    #20;
    aresetn <= 1;
    #400;
    $finish();
    
  end
 
endmodule 