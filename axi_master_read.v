module axi_master_read( aclk, aresetn, araddr, arready, arvalid,
                  rdata, rvalid, rready);
  
  input aclk;
  input aresetn;
  input arready;
  output rready;
  
  output araddr;
  output arvalid;
  input rdata;
  input rvalid;
  
  parameter ADDR_WIDTH = 32;
  parameter DATA_WIDTH = 32;
  parameter MEM_WIDTH = 32;
  parameter MEM_DEPTH = 1024;
  
  parameter IDLE=4'b0001;
  parameter ADDR=4'b0010;
  parameter DATA=4'b0100;
  
  wire aclk;
  wire aresetn;
  wire arready;
  reg rready;
  
  reg [ADDR_WIDTH-1:0] araddr;
  wire [DATA_WIDTH-1:0] rdata;
  reg arvalid;
  wire rvalid;
  
  reg [3:0] state;
  reg [3:0] next_state;
  
  reg trans_start;
  reg addr_done;
  reg data_done;
  
  
  reg [MEM_WIDTH-1:0] mem_wdata;
  reg [ADDR_WIDTH-1:0] mem_waddr;
  reg [MEM_WIDTH-1:0] memory [MEM_DEPTH];
  
  integer i;
  initial begin
    trans_start <= 1;
    addr_done <= 0;
    data_done <= 0;
    for( i = 0; i<MEM_DEPTH;i = i+1) begin
      memory[i] <= 0;
    end
  end
  
  
  
  
  always@(*) begin
    memory[mem_waddr] <= mem_wdata;
  end
  
  
  always@( posedge aclk, negedge aresetn ) begin
    if( !aresetn ) begin
      state <= IDLE;
    end else begin
      state <= next_state;
    end
  end
  
  always@(state or trans_start or addr_done or data_done or arready or rvalid) begin
    case(state) 
      IDLE: begin
        rready <= 0;
        data_done <= 0;
        next_state <= (trans_start)?ADDR : IDLE;
      end
      ADDR: begin
        araddr <= mem_waddr;
        arvalid <= 1;
        if( arready ) begin
          addr_done <= 1;
        end else begin
          addr_done <= 0;
        end
        next_state <= (addr_done )? DATA : ADDR;
      end
      DATA: begin
        mem_wdata <= rdata;
        arvalid <= 0;
        rready <= 1;
        if(  rvalid ) begin
          data_done <= 1;
        end else begin
          data_done <= 0;
        end
        next_state <= (data_done )? IDLE : DATA;
      end
      default : begin
        next_state <= IDLE;
      end
    endcase 
  end
  
  always@( posedge aclk or negedge aresetn ) begin
    if( !aresetn ) begin
      mem_waddr <= 0;
    end else begin
      if( data_done ) begin
        mem_waddr <= mem_waddr + 1;
      end else begin
        mem_waddr <= mem_waddr;
      end
    end
  end
  
endmodule 
 