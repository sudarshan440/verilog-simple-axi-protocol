module axi_master_write( aclk, aresetn, awaddr, awready, awvalid,
                  wdata, wvalid, wready, bresp, bvalid, bready);
  
  input aclk;
  input aresetn;
  input awready;
  input wready;
  input bresp;
  input bvalid;
  
  output awaddr;
  output awvalid;
  output wdata;
  output wvalid;
  output bready;
  
  parameter BRESP_WIDTH = 2;
  parameter ADDR_WIDTH = 32;
  parameter DATA_WIDTH = 32;
  parameter MEM_WIDTH = 32;
  parameter MEM_DEPTH = 1024;
  
  parameter IDLE=4'b0001;
  parameter ADDR=4'b0010;
  parameter DATA=4'b0100;
  parameter RESP=4'b1000;
  
  wire aclk;
  wire aresetn;
  wire awready;
  wire wready;
  wire [BRESP_WIDTH-1:0] bresp;
  wire bvalid;
  
  reg [ADDR_WIDTH-1:0] awaddr;
  reg [DATA_WIDTH-1:0] wdata;
  reg awvalid;
  reg wvalid;
  reg bready;
  
  reg [3:0] state;
  reg [3:0] next_state;
  
  reg trans_start;
  reg addr_done;
  reg data_done;
  reg resp_done;
  reg [BRESP_WIDTH-1:0] temp_resp;
  
  reg [MEM_WIDTH-1:0] mem_rdata;
  reg [ADDR_WIDTH-1:0] mem_raddr;
  reg [MEM_WIDTH-1:0] memory [MEM_DEPTH];
  
  integer i;
  initial begin
    trans_start <= 1;
    addr_done <= 0;
    data_done <= 0;
    resp_done <= 0;
    for( i = 0; i<MEM_DEPTH;i = i+1) begin
      memory[i] <= i;
    end
  end
  
  
  
  
  always@(*) begin
    mem_rdata <= memory[mem_raddr];
  end
  
  
  always@( posedge aclk, negedge aresetn ) begin
    if( !aresetn ) begin
      state <= IDLE;
    end else begin
      state <= next_state;
    end
  end
  
  always@(state or trans_start or addr_done or data_done or resp_done or awready or wready or bvalid) begin
    case(state) 
      IDLE: begin
        resp_done <= 0;
        bready <= 0;
        next_state <= (trans_start)?ADDR : IDLE;
      end
      ADDR: begin
        awaddr <= mem_raddr;
        awvalid <= 1;
        if( awready ) begin
          addr_done <= 1;
        end else begin
          addr_done <= 0;
        end
        next_state <= (addr_done )? DATA : ADDR;
      end
      DATA: begin
        wdata <= mem_rdata;
        awvalid <= 0;
        wvalid <= 1;
        if(  wready ) begin
          data_done <= 1;
        end else begin
          data_done <= 0;
        end
        next_state <= (data_done )? RESP : DATA;
      end
      RESP: begin
        wvalid <= 0;
        bready <= 1;
        if( bvalid  ) begin
          temp_resp <= bresp;
          resp_done <= 1;
        end else begin
          resp_done <= 0;
        end
        next_state <= (resp_done )? IDLE : RESP;
      end
      default : begin
        next_state <= IDLE;
      end
    endcase 
  end
  
  always@( posedge aclk or negedge aresetn ) begin
    if( !aresetn ) begin
      mem_raddr <= 0;
    end else begin
      if( resp_done ) begin
        mem_raddr <= mem_raddr + 1;
      end else begin
        mem_raddr <= mem_raddr;
      end
    end
  end
  
endmodule 
 